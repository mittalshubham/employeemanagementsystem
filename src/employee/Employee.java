package employee;

/* Employee Details Class.
 * In this class we are storing a employee's data
 */
public class Employee {
	private String employeeName; // Variable to store employee name
	private int employeeAge; // Variable to store employee age
	private int employeeId; // Variable to store unique id of the employee
	private String designation; // Variable to store designation of the employee
	public static int numberOfCEOs = 0;
	public int employeeMentor; // Variable to store id of the mentor of an employee
	public String designationTypes[] = { "ceo", "hr", "director", "manager", "fresher" }; // List of all the available
																							// designation

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;

	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public void setEmployeeAge(int employeeAge) {
		this.employeeAge = employeeAge;
	}

	public Employee(int employeeId) {
		this.employeeId = employeeId;
	}

	public Employee() {
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

}
