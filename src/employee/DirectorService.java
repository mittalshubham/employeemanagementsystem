package employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

//Director functional/service class
public class DirectorService extends ManagerService {

	public void promote(int employeeId, int promoteEmployeeId, ResultSet rs, PreparedStatement pstmt, Connection connection) throws SQLException {
		int employee = DataBaseQueries.Queries.retriveDesignation(employeeId, rs, pstmt, connection);
		//System.out.println(6);
		if (employee == 3 || employee == 2 || employee == 1) {
			//System.out.println(7);
			int currentDesignation = DataBaseQueries.Queries.retriveDesignation(promoteEmployeeId, rs, pstmt, connection);
			if (currentDesignation == 5) {
				//System.out.println(8);
				DataBaseQueries.Queries.updateDesignation(promoteEmployeeId, currentDesignation-1, rs, pstmt, connection);
				//employeeToPromote.setDesignation("manager");
				System.out.println("Fresher promoted to Manager");
			} else {
				super.promote(employeeId, promoteEmployeeId, rs, pstmt, connection);
			}
		} else {
			super.promote(employeeId, promoteEmployeeId, rs, pstmt, connection);
		}
	}

	public void addToTeam(int employeeId, int employeeToAddId, int mentorEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException{
		int employee = DataBaseQueries.Queries.retriveDesignation(employeeId, rs, pstmt, connection);
		if (employee ==3 || employee == 1 || employee == 2) {
			int mentorEmployee = DataBaseQueries.Queries.retriveDesignation(mentorEmployeeId, rs, pstmt, connection);
			if (mentorEmployee == 3) {
				int employeeToAdd = DataBaseQueries.Queries.retriveDesignation(employeeToAddId, rs, pstmt, connection);
				if (employeeToAdd == 1 || employeeToAdd == 3 || employeeToAdd == 2) {
					System.out.println("Can not perform this task");
				} else {
					System.out.println(100);
					DataBaseQueries.Queries.updateMentorId(employeeToAddId, mentorEmployeeId, rs, pstmt, connection);
//					List<Integer> mentorTeam = Team.get(mentor.getEmployeeId());
//					mentorTeam.add(employeeToAdd.getEmployeeId());
//					if (employeeToAdd.employeeMentor != 0) {
//						int currentMentorId = employeeToAdd.employeeMentor;
//						List<Integer> currentMemberList = Team.get(currentMentorId);
//						currentMemberList.remove(employeeToAdd.getEmployeeId());
//					}
//					employeeToAdd.employeeMentor = mentor.getEmployeeId();
					System.out.println(100);
					System.out.println("Task Completed");
				}
			}
		} else {
			super.addToTeam(employeeId, employeeToAddId, mentorEmployeeId, pstmt, rs, connection);
		}

	}

}
