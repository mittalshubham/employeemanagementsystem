package employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

// Interface for employee services.

public interface EmployeeServices {

	// To promote an employee
	public void promote(int employeeId, int promoteEmployeeId, ResultSet rs, PreparedStatement pstmt, Connection connection) throws SQLException;

	// To assign an employee a supervisor
	public void addToTeam(int employeeId, int employeeToAddId, int mentorEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException;

}
