package employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//Fresher functional/service class
public class FresherService implements EmployeeServices {

	public void promote(int employeeId, int promoteEmployeeId, ResultSet rs, PreparedStatement pstmt, Connection connection) throws SQLException{
		System.out.println("You do not have permission to promote");

	}

	public void addToTeam(int employeeId, int employeeToAddId, int mentorEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException{
		System.out.println("You don't have access to perform this task");

	}

	public void employeeWorkingUnderAnEmployee(int employeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException{
		int employee = DataBaseQueries.Queries.retriveDesignation(employeeId, rs, pstmt, connection);
		if (employee == 2) {
			System.out.println("HR has no team");
		} else if (employee == 5) {
			System.out.println("Fresher has no team");
		} else {
			int teamSize = DataBaseQueries.Queries.getMentorTeamSize(employeeId, pstmt, rs, connection);
			System.out.println("Number employees under this employee is : " + teamSize);
			DataBaseQueries.Queries.employeesUnderMentor(employeeId, pstmt, rs, connection);
		}

	}

}
