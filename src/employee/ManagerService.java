package employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

//Manager functional/service class
public class ManagerService extends FresherService {

	public void promote(int employeeId, int promoteEmployeeId, ResultSet rs, PreparedStatement pstmt, Connection connection) throws SQLException {
		super.promote(employeeId, promoteEmployeeId, rs, pstmt, connection);
	}

	public void addToTeam(int employeeId, int employeeToAddId, int mentorEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException{
		int employee = DataBaseQueries.Queries.retriveDesignation(employeeId, rs, pstmt, connection);
		if (employee == 4|| employee == 3 || employee == 1 || employee == 2) {
			int mentorEmployee = DataBaseQueries.Queries.retriveDesignation(mentorEmployeeId, rs, pstmt, connection);
			if (mentorEmployee == 4) {
				int employeeToAdd = DataBaseQueries.Queries.retriveDesignation(employeeToAddId, rs, pstmt, connection);
				if (employeeToAdd == 4 || employeeToAdd == 1 || employeeToAdd == 3 || employeeToAdd == 2) {
					System.out.println("Can not perform this task");
				} else {
					DataBaseQueries.Queries.updateMentorId(employeeToAddId, mentorEmployeeId, rs, pstmt, connection);
//					List<Integer> mentorTeam = Team.get(mentor.getEmployeeId());
//					mentorTeam.add(employeeToAdd.getEmployeeId());
//					if (employeeToAdd.employeeMentor != 0) {
//						int currentMentorId = employeeToAdd.employeeMentor;
//						List<Integer> currentMemberList = Team.get(currentMentorId);
//						currentMemberList.remove(employeeToAdd.getEmployeeId());
//					}
//					employeeToAdd.employeeMentor = mentor.getEmployeeId();
					System.out.println("Task Completed");
				}
			}
		} else {
			super.addToTeam(employeeId, employeeToAddId, mentorEmployeeId, pstmt, rs, connection);
		}
	}

}
