package employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBaseQueries {

	public static interface Queries {
		public static final String INSERT_EMPLOYEEs = "INSERT INTO `example`.`employee` (" + "  `name`," + "  `age`,"
				+ "  `doj`," + "  `created_at`," + "  `updated_at`," + "  `salary`," + "  `address_id`" + ") "
				+ "VALUES" + "  (" + "    ?," + "    ?," + "    ?," + "    NOW()," + "    NOW()," + "    ?," + "    ?"
				+ "  ) ";;

		public static final String INSERT_EMPLOYEE = "insert into Employee(name,age,designation_id,created_at,updated_at) values(?,?,?,NOW(),NOW())";

		public static final String QUERY1 = "select department.name from employee left join department on department.id=employee.department_id group by department.name";

		public static final String GET_DESIGNATION = "select name from Designation where id=1";

		public static boolean employeeIdValidation(int id, ResultSet rs, PreparedStatement pstmt, Connection connection)
				throws SQLException {
			pstmt = connection.prepareStatement("select id from Employee where id='" + id + "';");
			rs = pstmt.executeQuery();
			return rs.next();
		}

		public static int retriveDesignation(int employeeId, ResultSet rs, PreparedStatement pstmt,
				Connection connection) throws SQLException {
			System.out.println(employeeId);
			pstmt = connection.prepareStatement("select status,designation_Id from Employee where id='" + employeeId + "';");
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) == 0) {
				System.out.println("This employee no more works for the company");
				return 0;
			}
//			pstmt = connection.prepareStatement("select designation_id from Employee where id='" + employeeId + "';");
//			rs = pstmt.executeQuery();
//			rs.next();
			int mentorId = rs.getInt(2);
			return mentorId;
		}

		public static void updateDesignation(int employeeId, int newDesignationId, ResultSet rs,
				PreparedStatement pstmt, Connection connection) throws SQLException {
			pstmt = connection.prepareStatement("select status from Employee where id='" + employeeId + "';");
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) == 0) {
				System.out.println("This employee no more works for the company");
				return ;
			}
			pstmt = connection.prepareStatement("Update Employee set designation_id = ?, updated_at = NOW() where id = ?;");
			pstmt.setInt(1, newDesignationId);
			pstmt.setInt(2, employeeId);
			int numero = pstmt.executeUpdate();
		}

		public static int getMentorTeamSize(int employeeId, PreparedStatement pstmt, ResultSet rs,
				Connection connection) throws SQLException {
			pstmt = connection.prepareStatement("select status from Employee where id='" + employeeId + "';");
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) == 0) {
				System.out.println("This employee no more works for the company");
				return 0;
			}
			pstmt = connection.prepareStatement("select count(*) from Employee where mentor='" + employeeId + "';");
			rs = pstmt.executeQuery();
			rs.next();
			int teamSize = rs.getInt(1);
			return teamSize;
		}

		public static void updateMentorId(int employeeId, int newMentorId, ResultSet rs, PreparedStatement pstmt,
				Connection connection) throws SQLException {
			pstmt = connection.prepareStatement("select status from Employee where id='" + employeeId + "';");
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) == 0) {
				System.out.println("This employee no more works for the company");
				return;
			}
			pstmt = connection.prepareStatement("select status from Employee where id='" + newMentorId + "';");
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) == 0) {
				System.out.println("This employee no more works for the company");
				return;
			}
			pstmt = connection.prepareStatement("Update Employee set mentor = ?,updated_at=NOW() where id = ?;");
			pstmt.setInt(1, newMentorId);
			pstmt.setInt(2, employeeId);
			int numero = pstmt.executeUpdate();
		}

		public static void employeesUnderMentor(int employeeId, PreparedStatement pstmt, ResultSet rs,
				Connection connection) throws SQLException {
			pstmt = connection.prepareStatement("select id from Employee where mentor='" + employeeId + "' and status = 1;");
			rs = pstmt.executeQuery();
			if (!(rs.next())) {
				System.out.println("No employee working under this employeeId");
				return;
			}
			System.out.println("Ids of each employees are :");
			while (rs.next()) {
				System.out.println(rs.getInt(1));
			}
		}

		public static Employee employeeDetails(int employeeId, PreparedStatement pstmt, ResultSet rs,
				Connection connection) throws SQLException {
			Employee employee = new Employee();
			pstmt = connection.prepareStatement(
					"select Employee.name, Employee.age, Designation.name from Designation inner join Employee on Employee.designation_id=Designation.id where Employee.designation_id='"
							+ employeeId + "' and Employee.status = 1;");
			rs = pstmt.executeQuery();
			if(!(rs.next())){
				System.out.println("Employee no more works in the company");
			}
			rs.next();
			employee.setEmployeeName(rs.getString(1));
			employee.setEmployeeAge(rs.getInt(2));
			employee.setDesignation(rs.getString(3));
			return employee;
		}

		public static void getTeamMembers(int employeeId, PreparedStatement pstmt, ResultSet rs, Connection connection)
				throws SQLException {
			pstmt = connection
					.prepareStatement("select id from Employee where mentor in (select mentor from Employee where id ='"
							+ employeeId + "' and status = 1);");
			rs = pstmt.executeQuery();
			if (!(rs.next())) {
				System.out.println("This employee has no team member/mentor");
			} else {
				System.out.println("Ids of each employees are :");
				while (rs.next()) {
					System.out.println(rs.getInt(1));
				}
			}
		}

		public static void employeeMentorId(int employeeId, PreparedStatement pstmt, ResultSet rs,
				Connection connection) throws SQLException {
			pstmt = connection
					.prepareStatement("select mentor from Employee where id ='" + employeeId + "' and status = 1;");
			rs = pstmt.executeQuery();
			if (rs.next()) {
				System.out.print("Mentor Id is : ");
				System.out.println(rs.getInt(1));
			} else {
				System.out.println("This employee has no mentor");
			}
		}

		public static void employeeDelete(int employeeId, PreparedStatement pstmt, ResultSet rs, Connection connection)
				throws SQLException {
			pstmt = connection.prepareStatement("Update Employee set status = ? where id = ?;");
			pstmt.setInt(1, 0);
			pstmt.setInt(2, employeeId);
			int numero = pstmt.executeUpdate();
			pstmt = connection.prepareStatement("Update Employee set mentor = NULL where mentor = ?;");
			pstmt.setInt(1, employeeId);
			int numer = pstmt.executeUpdate();
		}
	}
}
