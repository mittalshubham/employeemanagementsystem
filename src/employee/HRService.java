package employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

// HR functional/service class
public class HRService extends DirectorService {

	public void promote(int employeeId, int promoteEmployeeId, ResultSet rs, PreparedStatement pstmt, Connection connection) throws SQLException{
		int employee = DataBaseQueries.Queries.retriveDesignation(employeeId, rs, pstmt, connection);
		if (employee == 1 || employee == 2) {
			int currentDesignation = DataBaseQueries.Queries.retriveDesignation(promoteEmployeeId, rs, pstmt, connection);
			if (currentDesignation == 4) {
				DataBaseQueries.Queries.updateDesignation(promoteEmployeeId, currentDesignation-1, rs, pstmt, connection);
				//employeeToPromote.setDesignation("director");
				System.out.println("Manger promoted to Director");
			} else {
				super.promote(employeeId, promoteEmployeeId, rs, pstmt, connection);
			}
		} else {
			//System.out.println(5);
			super.promote(employeeId, promoteEmployeeId, rs, pstmt, connection);
		}
	}

	public void addToTeam(int employeeId, int employeeToAddId, int mentorEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException {
		int employee = DataBaseQueries.Queries.retriveDesignation(employeeId, rs, pstmt, connection);
		System.out.println("Employee"+employee);
		System.out.println("Employee"+employeeId);
		if (employee == 1 || employee == 2) {
			int mentorEmployee = DataBaseQueries.Queries.retriveDesignation(mentorEmployeeId, rs, pstmt, connection);
			System.out.println("Mentor"+mentorEmployee);
			System.out.println("Mentor"+mentorEmployeeId);
			if (mentorEmployee == 1) {
				int employeeToAdd = DataBaseQueries.Queries.retriveDesignation(employeeToAddId, rs, pstmt, connection);
				System.out.println("EmployeeToAdd"+employeeToAdd);
				System.out.println("EmployeeToAdd"+employeeToAddId);
				if (employeeToAdd == 1 || employeeToAdd  == 2) {
					System.out.println("Can not perform this task");
				} else {
					DataBaseQueries.Queries.updateMentorId(employeeToAddId, mentorEmployeeId, rs, pstmt, connection);
//					List<Integer> mentorTeam = Team.get(mentor.getEmployeeId());
//					mentorTeam.add(employeeToAdd.getEmployeeId());
//					if (employeeToAdd.employeeMentor != 0) {
//						int currentMentorId = employeeToAdd.employeeMentor;
//						List<Integer> currentMemberList = Team.get(currentMentorId);
//						currentMemberList.remove(employeeToAdd.getEmployeeId());
//					}
//					employeeToAdd.employeeMentor = mentor.getEmployeeId();
					System.out.println("Task Completed");
				}
			}
		} else {
			super.addToTeam(employeeId, employeeToAddId, mentorEmployeeId, pstmt, rs, connection);
		}

	}
}
