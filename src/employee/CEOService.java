package employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

// CEO functional/service class
public class CEOService extends HRService {

	public void promote(int employeeId, int promoteEmployeeId, ResultSet rs, PreparedStatement pstmt, Connection connection) throws SQLException {
		//System.out.println(4);
		super.promote(employeeId, promoteEmployeeId, rs, pstmt, connection);
	}

	public void addToTeam(int employeeId, int employeeToAddId, int mentorEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException {
		super.addToTeam(employeeId, employeeToAddId, mentorEmployeeId, pstmt, rs, connection);
	}
}
