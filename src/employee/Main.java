package employee;

import java.util.*;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.util.Date;
//import java.util.ResourceBundle;

//import com.girnarsoft.example.jdbc.Constants;

/*Main execution class of this
 *  employee management system
 */
public class Main {
	static Connection connection = null;

	public static void main(String[] args) throws SQLException {
		connection = Connect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int employeeId;

		Scanner scanner = new Scanner(System.in);
		Map<Integer, Employee> EmployeesMap = new HashMap<>();
		Map<Integer, List<Integer>> Team = new HashMap<>();
		int userChoice = 0;
		while (((userChoice = getUserChoice(scanner)) <= 8) && userChoice >= 1) {
			switch (userChoice) {
			case 1:
				createEmployee(scanner, pstmt, rs);
				break;

			case 2:
				System.out.println("Enter your employeeId");
				employeeId = checkIntegerInput(scanner);
				System.out.println("Enter the employeeId you want to promote");
				int promoteEmployeeId = checkIntegerInput(scanner);
				promoteEmployee(employeeId, promoteEmployeeId, pstmt, rs, connection);
				// System.out.println(1);
				break;

			case 3:
				System.out.println("Enter your employeeId");
				employeeId = checkIntegerInput(scanner);
				System.out.println("Enter the employee Id to whom you want to add in some team");
				int employeeToAddId = checkIntegerInput(scanner);
				System.out.println("Enter the Employee Id to whose team you want to add an employee");
				int mentorEmployeeId = checkIntegerInput(scanner);
				assignTeam(employeeId, employeeToAddId, mentorEmployeeId, pstmt, rs, connection);
				break;

			case 4:
				System.out.println("Enter the id of employee whose team you want to see");
				employeeId = checkIntegerInput(scanner);
				employeeWorkingUnder(employeeId, pstmt, rs, connection);
				break;

			case 5:
				System.out.println("Enter the employee id");
				employeeId = checkIntegerInput(scanner);
				if(!(DataBaseQueries.Queries.employeeIdValidation(employeeId, rs, pstmt, connection))) {
					System.out.println("Enter your valid id");
					return;
				}
				Employee employee = new Employee();
				employee = DataBaseQueries.Queries.employeeDetails(employeeId, pstmt, rs, connection);
				System.out.println("Name of Employee : " + employee.getEmployeeName());
				System.out.println("Age of Employee : " + employee.getEmployeeAge());
				System.out.println("Designation of Employee : " + employee.getDesignation());
				break;

			case 6:
				System.out.println("Enter the employee id");
				employeeId = checkIntegerInput(scanner);
				Employee employee5 = EmployeesMap.get(employeeId);
				if (employee5 == null) {
					System.out.println("Invalid id");
					continue;
				}
				if (employee5.employeeMentor == 0) {
					System.out.println("This employee has no mentor");
				} else {
					System.out.println("Id of Employee : " + employee5.employeeMentor);
				}
				break;

			case 7:
				System.out.println("Enter the employeeId");
				employeeId = checkIntegerInput(scanner);
				getTeamMembers(employeeId, pstmt, rs, connection);
				break;

			case 8:
				System.out.println("Enter the employeeId");
				employeeId = checkIntegerInput(scanner);
				deleteEmployee(employeeId,pstmt,rs,connection);

			default:
				System.out.println("Enter from the given choice");
				break;
			}
		}
		scanner.close();
		if (pstmt != null) {
			pstmt.close();
		}
		if (connection != null) {
			connection.close();
		}
		System.out.println("Closed");
	}

	private static int getUserChoice(Scanner scanner) {
		System.out.println("Choose from the given options");
		System.out.println("1. Create an employee");
		System.out.println("2. Promote an employee");
		System.out.println("3. Assign a team to a employee");
		System.out.println("4. Total employees working under an employee");
		System.out.println("5. Show the details of a particular employee");
		System.out.println("6. Show the mentor of an employee");
		System.out.println("7. Team Members");
		// System.out.println("8. Show Details Initially Stored In File");
		System.out.println("8. Delete employee");
		System.out.println("Any number to exit");
		return checkIntegerInput(scanner);
	}

	// Method to print the team members of a particular employee
	private static void getTeamMembers(int employeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException {
		if(!(DataBaseQueries.Queries.employeeIdValidation(employeeId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		DataBaseQueries.Queries.getTeamMembers(employeeId, pstmt, rs, connection);
	}

	/*
	 * This method is called when an new employee is being created. Here all the
	 * details of the employee is filled.
	 */
	private static void createEmployee(Scanner scanner, PreparedStatement pstmt, ResultSet rs) throws SQLException {

		Employee employee = new Employee();
		System.out.println("New employee is created with id " + employee.getEmployeeId());
		System.out.println("Enter the name of the employee");
		scanner.nextLine();
		employee.setEmployeeName(checkStringInput(scanner));
		System.out.println("Enter the designation for the employee");
		System.out.println("You can enter from");
		//System.out.println("ceo");
		System.out.println("hr");
		System.out.println("director");
		System.out.println("manager");
		System.out.println("fresher");
		while (true) {
			employee.setDesignation(checkStringInput(scanner));
			String enteredDesignation = employee.getDesignation().toLowerCase();
			employee.setDesignation(enteredDesignation);
			if (Arrays.asList(employee.designationTypes).contains(enteredDesignation)) {
				if (employee.getDesignation().equals("ceo")) {
					Employee.numberOfCEOs++;
					if (Employee.numberOfCEOs > 1) {
						System.out.println("Ceo already exists");
						Employee.numberOfCEOs = 1;
						continue;
					}
				}
				break;
			} else {
				System.out.println("Enter correct designation");
				continue;
			}
		}
		System.out.println("Enter the age of the employee b/w 18 and 65");
		while (true) {
			employee.setEmployeeAge(checkIntegerInput(scanner));
			int age = employee.getEmployeeAge();
			if (age >= 18 && age <= 65) {
				break;
			} else
				System.out.println("Enter age in range 18 and 65");
			continue;
		}

		try {
			connection.setAutoCommit(false);

			pstmt = connection.prepareStatement("select id from Designation where name='" + employee.getDesignation() + "';");
			rs = pstmt.executeQuery();
			rs.next();
			int id = rs.getInt(1);

			pstmt = connection.prepareStatement(DataBaseQueries.Queries.INSERT_EMPLOYEE,
					Statement.RETURN_GENERATED_KEYS);

			pstmt.setString(1, employee.getEmployeeName());
			pstmt.setInt(2, employee.getEmployeeAge());
			pstmt.setInt(3, id);

			int numero = pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();

			connection.commit();
			// System.out.println("address id " + insertedAddressId + ", employee id " +
			// insertedEmployeeIdKey);
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
		}
		System.out.println("Congratulations!");
		System.out.println("Employee with id " + employee.getEmployeeId() + " and name " + employee.getEmployeeName()
				+ " with designation " + employee.getDesignation() + " has been created");
		// EmployeesMap.put(employee.getEmployeeId(), employee);
		// Team.put(employeeId, new ArrayList<Integer>());

	}

	/*
	 * This method is called when an employee is to be promoted. User has to enter
	 * his/her employeeId and the id of the employee to be promoted.
	 */
	private static void promoteEmployee(int employeeId, int promoteEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException {
		
		if(!(DataBaseQueries.Queries.employeeIdValidation(employeeId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		
		if(!(DataBaseQueries.Queries.employeeIdValidation(promoteEmployeeId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		
		EmployeeServices service;
		service = new CEOService();
		// System.out.println(2);
		service.promote(employeeId, promoteEmployeeId, rs, pstmt, connection);
		// System.out.println(3);
	}

	/*
	 * This method is called when an employee is being assigned a supervisor. User
	 * has to enter his/her employee id, id of the supervisor and the id of the
	 * employee who is being assigned a team/supervisor.
	 */
	private static void assignTeam(int employeeId, int employeeToAddId, int mentorEmployeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException {
		
		if(!(DataBaseQueries.Queries.employeeIdValidation(employeeId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		
		if(!(DataBaseQueries.Queries.employeeIdValidation(employeeToAddId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		
		if(!(DataBaseQueries.Queries.employeeIdValidation(mentorEmployeeId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		
		EmployeeServices service1;
		service1 = new CEOService();
		service1.addToTeam(employeeId, employeeToAddId, mentorEmployeeId, pstmt, rs, connection);
	}

	// This method is called when the user wants to know the all the employees
	// working under him/her.
	private static void employeeWorkingUnder(int employeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException {
		if(!(DataBaseQueries.Queries.employeeIdValidation(employeeId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		FresherService fresherService = new FresherService();
		fresherService.employeeWorkingUnderAnEmployee(employeeId, pstmt, rs, connection);
	}

	// To delete an employee.
	private static void deleteEmployee(int employeeId, PreparedStatement pstmt, ResultSet rs, Connection connection) throws SQLException {
		if(!(DataBaseQueries.Queries.employeeIdValidation(employeeId, rs, pstmt, connection))) {
			System.out.println("Enter your valid id");
			return;
		}
		DataBaseQueries.Queries.employeeDelete(employeeId, pstmt, rs, connection);
		System.out.println("SuccessFully deleted the employee with id = "+employeeId);
	}

	// Validates the interger input entered by the user.
	private static int checkIntegerInput(Scanner scanner) {
		int userInput = 0;
		boolean done = false;
		while (!done) {
			if (scanner.hasNextInt()) {
				userInput = scanner.nextInt();
				done = true;
			} else {
				// consume the non-int
				scanner.next();
				System.out.println("Please input an Integer");
			}
		}
		return userInput;
	}

	// Validates the string input entered by the user.
	private static String checkStringInput(Scanner scanner) {
		String userInput = new String();
		while (true) {
			while (!scanner.hasNext("[A-Za-z]+")) {
				scanner.nextLine();
				System.out.println("Wrong Type of Input");
			}
			userInput = scanner.nextLine();
			if (userInput.isEmpty()) {
				continue;
			} else
				return userInput;
		}
	}

}
